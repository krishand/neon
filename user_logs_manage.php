

<div class="row">

    <div class="col-md-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="col-md-9"><?php echo $title;?></h2>


            </header>

            <div class="main-box-body clearfix">
                <div class="col-md-12" style="margin-bottom: 10px; " >
                    <div class="col-md-1">By Word:</div>
                    <div class="col-md-3"><input class="form-control" id="filter" type="text"/></div>

                    <form class="form-horizontal" role="form" name="log_search" id="log_search">

                    <div class="col-md-1">By User:</div>
                    <div class="col-md-3"><select class="form-control" name="user_id" id="user_id">
                            <option value="">-- User--</option>
                            <?php
                            foreach ($user_ids as $user_id) {

                                ?>
                                <option
                                    value="<?php echo $user_id->user_id; ?>"><?php echo $user_id->vFirstName.' '.$user_id->vLastName; ?></option>

                            <?php

                            }
                            ?>
                        </select></div>


                    <div class="col-md-1"> By Action:</div>
                    <div class="col-md-3"><select class="form-control" name="action" id="action">
                            <option value="">-- Action--</option>
                            <?php
                            foreach ($actions as $action) {

                                ?>
                                <option
                                    value="<?php echo $action->action; ?>"><?php echo $action->action ; ?></option>

                            <?php

                            }
                            ?>
                        </select> </div>


                    <div class="col-md-1"> By Ip:</div>
                    <div class="col-md-3"><select class="form-control" name="ipnum" id="ipnum">
                            <option value="">-- IP --</option>
                            <?php
                            foreach ($ips as $ip) {

                                ?>
                                <option
                                    value="<?php echo $ip->ip; ?>"><?php echo $ip->ip ; ?></option>

                            <?php

                            }
                            ?>
                        </select> </div>


                    <div class="col-md-1">Date Range</div>
                    <div class="col-md-3"><input class="form-control" id="datefrom" name="datefrom" type="text"/><input class="form-control" id="dateto" name="dateto" type="text"/> </div>
                        <div class="col-md-1"> </div>
                    <div class="col-md-3"> <button class="btn btn-info">Submit</button></div>

                        </form>
                    <div class="col-md-3"><img src="<?php echo base_url();?>/application_assets/images/icons/color/doc_excel_csv.png"> </div>


                </div>




                <div class="col-md-12">
                    <table class="footable table viewtbl" data-filter="#filter" data-filter-text-only="true">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th data-hide="all" >Main</th>
                            <th >Action</th>
                            <th >Date</th>
                            <th data-hide="phone,tablet">Branch Name</th>
                            <th data-hide="phone,tablet">Group</th>
                            <th data-hide="phone,tablet">IP</th>
                        </tr>
                        </thead>

                        <tbody id="log_search_view">
                        <?php
                        $a = 0;

                        foreach ($user_logs as $user_log) {
                            $a++;
                            ?>
                            <tr>
                                <td><?php echo $a; ?></td>
                                <td><?php echo $user_log->vFirstName . ' ' . $user_log->vLastName; ?></td>
                                <td><?php echo $user_log->uri; ?></td>
                                <td><?php echo $user_log->action;
                                    if($user_log->post_data != ''){ ?>
                                        <span style="color: red;"> Data Post</span>
                                        <a data-toggle="modal" href="#viewData" class="md-trigger mrg-b-lg"
                                       data-modal="modal-1" title="View Post Data">
                                        <input type="hidden" value="<?php echo $user_log->id; ?>">
                                         <img
                                                src="<?php echo base_url(); ?>/application_assets/images/icons/color/magnifier.png"
                                                id="view_data"/> </a>
                                    <?php } ?></td>
                                <td><?php echo  $user_log->date ; ?></td>
                                <td><?php echo $user_log->vCompanyName . ' - ' . $user_log->vBranchName; ?></td>
                                <td><?php echo $user_log->UserGroup; ?></td>
                                <td><?php echo $user_log->ip; ?></td>

                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        <tfoot class="hide-if-no-paging">
                        <tr>
                            <td colspan="12">
                                <div class="pagination pagination-centered"></div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="viewData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Post Data</h4>
            </div>
            <div class="modal-body">
                <div id="log_view_data"></div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">




    $(document).ready(function () {

        $('#log_search').validate({

            submitHandler: function (form) {


                $.post(js_site_url + 'master_controller/search_log', $('#log_search').serialize(), function (msg) {

                    if (msg != '') {

                        $("#log_search_view").html(msg);
                        $('table').trigger('footable_redraw');
                    } else {
                        $("#log_search_view").html('<div  class="alert alert-danger" ><i class="fa fa-times-circle fa-fw fa-lg"></i> An Error Occurred - ' + msg + '</div>');
                    }


                });


            }
        });

    });




    $(document).on('click', '#view_data', function () {


        var log_id = $(this).prev().val();

        $.post(js_site_url + 'master_controller/viewPost', {log_id: log_id}, function (msg) {

            if (msg != 0) {
                $("#log_view_data").html(msg);

            } else {
                $("#frmmsg").html('<div class="alert alert-danger">An error occurred.' + msg + '</div> ');
            }


        });


    });

</script>